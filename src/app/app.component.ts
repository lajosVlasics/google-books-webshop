import { Component, OnInit } from '@angular/core';

import { CartService } from './services/cart.service';
import { PageType } from './models/page-type';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public pageType = PageType;

  public selectedPage: PageType = PageType.SEARCH;

  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.cartService.fetchData();
  }

  setPage(page: PageType) {
    this.selectedPage = page;
  }

}
