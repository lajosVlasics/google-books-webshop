import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BookDetailsModalComponent } from './components/modal/book-details-modal/book-details-modal.component';
import { CartComponent } from './components/pages/cart/cart.component';
import { CartService } from './services/cart.service';
import { GoogleBooksService } from './services/google-books.service';
import { HeaderComponent } from './components/header/header.component';
import { CartDataLocalStorageService } from './services/cart-data-local-storage.service';
import { SearchComponent } from './components/pages/search/search.component';
import { SearchInputComponent } from './components/pages/search/search-input/search-input.component';
import { SearchListComponent } from './components/pages/search/search-list/search-list.component';
import { SearchPagerComponent } from './components/pages/search/search-pager/search-pager.component';
import { SearchService } from './services/search.service';

@NgModule({
  declarations: [
    AppComponent,
    BookDetailsModalComponent,
    CartComponent,
    HeaderComponent,
    SearchComponent,
    SearchInputComponent,
    SearchListComponent,
    SearchPagerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: 'CartDataService', useClass: CartDataLocalStorageService },
    CartService,
    GoogleBooksService,
    SearchService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
