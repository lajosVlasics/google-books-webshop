import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { CartService } from './../../services/cart.service';
import { PageType } from './../../models/page-type';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() pageSelectedEvent: EventEmitter<PageType> = new EventEmitter<PageType>();

  public pageType = PageType;

  constructor(public cartService: CartService) { }

  ngOnInit() {
  }

  onPageSelect(selectedPage: PageType) {
    this.pageSelectedEvent.emit(selectedPage);
  }

}
