import { Component, OnInit, ViewChild, ContentChild } from '@angular/core';

import { CartService } from './../../../services/cart.service';
import { Book } from 'src/app/models/book.model';

declare var $: any;

@Component({
  selector: 'app-book-details-modal',
  templateUrl: './book-details-modal.component.html',
  styleUrls: ['./book-details-modal.component.css']
})
export class BookDetailsModalComponent implements OnInit {

  public book: Book;

  public showAddToCartFooter: boolean;

  @ViewChild('bookDetailsModal') modal: ContentChild;

  constructor(private cartService: CartService) { }

  ngOnInit() {
  }

  public show(book: Book) {
    this.book = book;
    this.showAddToCartFooter = false;
    $('#bookDetailsModal').modal('show');
  }

  public showWithAddCartFooter(book: Book) {
    this.book = book;
    this.showAddToCartFooter = true;
    $('#bookDetailsModal').modal('show');
  }

  public onAddToCart(book: Book) {
    this.cartService.addBook(book);
  }

}
