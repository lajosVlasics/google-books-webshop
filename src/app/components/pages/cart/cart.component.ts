import { Component, OnInit, ViewChild } from '@angular/core';

import { Book } from './../../../models/book.model';
import { BookDetailsModalComponent } from './../../modal/book-details-modal/book-details-modal.component';
import { CartService } from './../../../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {

  @ViewChild(BookDetailsModalComponent) bookDetailsModal: BookDetailsModalComponent;

  constructor(public cartService: CartService) { }

  ngOnInit() {
  }

  public onBookSelect(book: Book) {
    this.bookDetailsModal.show(book);
  }

  public onAddBook(book: Book, event: Event) {
    event.stopPropagation();
    this.cartService.addBook(book);
  }

  public onRemoveBook(book: Book, event: Event) {
    event.stopPropagation();
    this.cartService.removeBook(book);
  }

}
