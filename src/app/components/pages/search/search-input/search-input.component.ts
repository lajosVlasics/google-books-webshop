import { Component, OnInit } from '@angular/core';

import { SearchService } from './../../../../services/search.service';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements OnInit {

  public searchInput: string;

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchInput = this.searchService.getSearchTitle();
  }

  public onSearchBtn() {
    this.searchService.newSearchByTitle(this.searchInput);
  }

}
