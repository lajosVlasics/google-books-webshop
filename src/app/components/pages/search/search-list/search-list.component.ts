import { Component, OnInit, ViewChild } from '@angular/core';

import { Book } from './../../../../models/book.model';
import { BookDetailsModalComponent } from './../../../modal/book-details-modal/book-details-modal.component';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {

  @ViewChild(BookDetailsModalComponent) bookDetailsModal: BookDetailsModalComponent;

  constructor(public searchService: SearchService) { }

  ngOnInit() {
  }

  public onBookSelect(book: Book) {
    this.bookDetailsModal.showWithAddCartFooter(book);
  }

}
