import { Component, OnInit } from '@angular/core';

import { SearchService } from './../../../../services/search.service';

@Component({
  selector: 'app-search-pager',
  templateUrl: './search-pager.component.html',
  styleUrls: ['./search-pager.component.css']
})
export class SearchPagerComponent implements OnInit {

  constructor(private searchService: SearchService) { }

  public currentPage: number;

  public totalPages: number;

  public pages: number[];

  public startPage: number;

  public endPage: number;

  private pageSize = 20;

  ngOnInit() {
    this.currentPage = this.searchService.getSearchPageOffset();

    if (this.searchService.getTotalItems()) {
      this.initPagerParameters(this.searchService.getTotalItems(), false);
    }

    this.searchService.searchEvent.subscribe(
      (isNewTitleSearch: boolean) =>
        this.initPagerParameters(this.searchService.getTotalItems(), isNewTitleSearch)
    );
  }

  public setPage(page: number) {
    this.currentPage = page;
    this.searchService.existingSearchByPageOffset(page, this.pageSize);
  }

  private initPagerParameters(totalItems: number, isNewSearch: boolean) {
    this.totalPages = Math.ceil(totalItems / this.pageSize);
    this.currentPage = isNewSearch ? 1 : this.currentPage;

    if (this.currentPage < 1) {
      this.currentPage = 1;
    } else if (this.currentPage > this.totalPages) {
      this.currentPage = this.totalPages;
    }

    if (this.totalPages <= 10) {
      this.startPage = 1;
      this.endPage = this.totalPages;
    } else {
      if (this.currentPage <= 6) {
        this.startPage = 1;
        this.endPage = 10;
      } else if (this.currentPage + 4 >= this.totalPages) {
        this.startPage = this.totalPages - 9;
        this.endPage = this.totalPages;
      } else {
        this.startPage = this.currentPage - 5;
        this.endPage = this.currentPage + 4;
      }
    }

    this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
  }
}
