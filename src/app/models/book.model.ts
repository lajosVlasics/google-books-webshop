export class Book {
  public id: string;
  public title: string;
  public categories: string[];
  public authors: string[];
  public pageCount: number;
  public thumbnail: string;
  public smallThumbnail: string;
  public description: string;
}
