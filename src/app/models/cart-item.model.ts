import { Book } from './book.model';

export class CartItem {

  public book: Book;
  public quantity: number;

}
