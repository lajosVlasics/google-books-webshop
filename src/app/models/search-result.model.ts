import { Book } from './book.model';

export class SearchResult {

  public totalItems: number;
  public books: Book[];

}
