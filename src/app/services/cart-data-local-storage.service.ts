import { Injectable } from '@angular/core';

import { CartDataService } from './interfaces/cart-data-service.interface';
import { CartItem } from '../models/cart-item.model';

@Injectable()
export class CartDataLocalStorageService implements CartDataService {

  constructor() {}

  public writeCartData(cartItems: CartItem[]) {
    localStorage.setItem('cart', JSON.stringify(cartItems));
  }

  public retrieveCartData(): CartItem[] {
    return JSON.parse(localStorage.getItem('cart'));
  }

}
