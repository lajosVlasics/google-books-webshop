import { Injectable, Inject } from '@angular/core';

import { Book } from './../models/book.model';
import { CartItem } from '../models/cart-item.model';
import { CartDataLocalStorageService } from './cart-data-local-storage.service';

@Injectable()
export class CartService {

  private items: CartItem[] = [];

  constructor(
    @Inject('CartDataService') private dataService: CartDataLocalStorageService) { }

  public getItems(): CartItem[] {
    return this.items;
  }

  public getTotalItemsQuantity(): number {
    let sum = 0;
    this.items.forEach(i => sum += i.quantity);
    return sum;
  }

  public addBook(book: Book) {
    const cartItem = this.items.find(i => i.book.id === book.id);
    if (cartItem) {
      cartItem.quantity += 1;
    } else {
      this.items.push({
        book: book,
        quantity: 1
      });
    }

    this.dataService.writeCartData(this.items);
  }

  public removeBook(book: Book) {
    const cartItem = this.items.find(i => i.book.id === book.id);
    if (cartItem.quantity > 1) {
      cartItem.quantity -= 1;
    } else {
      this.items.splice(this.items.indexOf(cartItem), 1);
    }

    this.dataService.writeCartData(this.items);
  }

  public fetchData() {
    const fetchedItems = this.dataService.retrieveCartData();
    if (fetchedItems) {
      this.items = fetchedItems;
    }
  }

}
