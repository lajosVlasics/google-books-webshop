import { SearchResult } from './../models/search-result.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Book } from './../models/book.model';

@Injectable()
export class GoogleBooksService {

  constructor(private http: HttpClient) { }

  public searchByTitleAndOffset(title: string, offset: number, maxResults: number): Observable<SearchResult> {
    const url = 'https://www.googleapis.com/books/v1/volumes?q=intitle:' + title + '&startIndex=' + offset + '&maxResults=' + maxResults;
    return this.http.get(url).pipe(
      map(this.toSearchResult)
    );
  }

  private toSearchResult(res: any): SearchResult {
    const searchedBooks: Book[] = [];
    if (res.totalItems > 0) {
      for (const book of res.items) {
        searchedBooks.push({
          id: book.id,
          title: book.volumeInfo.title,
          categories: book.volumeInfo.categories,
          authors: book.volumeInfo.authors,
          pageCount: book.volumeInfo.pageCount,
          thumbnail: book.volumeInfo.imageLinks ? book.volumeInfo.imageLinks.thumbnail : '',
          smallThumbnail: book.volumeInfo.imageLinks ? book.volumeInfo.imageLinks.smallThumbnail : '',
          description: book.volumeInfo.description,
        });
      }
    }
    return {
      totalItems: res.totalItems,
      books: searchedBooks
    };
  }

}
