import { CartItem } from './../../models/cart-item.model';

export interface CartDataService {

  writeCartData(cartItems: CartItem[]): void;

  retrieveCartData(): CartItem[];

}
