import { Injectable, EventEmitter } from '@angular/core';

import { Book } from '../models/book.model';
import { GoogleBooksService } from './google-books.service';

@Injectable()
export class SearchService {

  constructor(private googleBooksService: GoogleBooksService) { }

  private searchTitle: string;

  private searchPageOffset = 1;

  private totalItems: number;

  private books: Book[];

  private loading = false;

  public searchEvent = new EventEmitter<boolean>();

  public getBooks(): Book[] {
    return this.books;
  }

  public getTotalItems(): number {
    return this.totalItems;
  }

  public getSearchTitle(): string {
    return this.searchTitle;
  }

  public getSearchPageOffset(): number {
    return this.searchPageOffset;
  }

  public isLoading(): boolean {
    return this.loading;
  }

  private searcByTitleAndOffset(title: string, pageOffset: number = 1, pageSize: number = 20) {
    const isNewTitleSearch = !(this.searchTitle === title);
    this.loading = true;
    this.searchTitle = title;
    this.searchPageOffset = pageOffset;
    this.googleBooksService.searchByTitleAndOffset(title, this.calculateOffsetByPage(pageOffset, pageSize), pageSize).subscribe(
      (p) => {
        if (isNewTitleSearch) {
          this.totalItems = p.totalItems;
        }
        this.books = p.books;
        this.loading = false;
        this.searchEvent.emit(isNewTitleSearch);
      },
      (e) => {
        console.log(e);
        this.books = [];
        this.loading = false;
      }
    );
  }

  public newSearchByTitle(title: string) {
    this.searchPageOffset = 1;
    this.searcByTitleAndOffset(title, this.searchPageOffset);
  }

  public existingSearchByPageOffset(pageOffset: number, pageSize: number) {
    this.searchPageOffset = pageOffset;
    this.searcByTitleAndOffset(this.searchTitle, pageOffset, pageSize);
  }

  private calculateOffsetByPage(page: number, pageSize: number): number {
    return page * pageSize - pageSize;
  }

}
